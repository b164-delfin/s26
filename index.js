const http = require('http');

const port = 3000;

const server = http.createServer((request, response) =>{

	if(request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('You are in the login page');

	}

	else if(request.url == '/register') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('The page cannot load.');
	}
})


server.listen(port);

console.log(`Server now accessible at localhost:${port}.`);